
maze = {
  '1,1' : ['2,1'],
  '1,2' : ['1,3','2,2'],
  '1,3' : ['1,2', '1,4'],
  '1,4' : ['1,3', '2,4'],
  '2,1' : ['3,1'],
  '2,2' : ['1,3', '2,3'],
  '2,3' : ['2,2'],
  '2,4' : ['1,4', '3,4'],
  '3,1' : ['4,1'],
  '3,2' : ['4,2', '3,3'],
  '3,3' : ['3,2', '3,4'],
  '3,4' : ['2,4', '4,4', '3,3'],
  '4,1' : ['3,1', '4,2'],
  '4,2' : ['3,2', '4,1'],
  '4,3' : ['4,4'],
  '4,4' : ['3,4', '4,3']
}

def checkconnect():
  if '4,1' in maze['3,1']:
      print("Connected")
  else:
     print("Not Connected")

checkconnect()


def bfs(graph, root, query):
    # the queue is a list of lists of nodes
    # each list in the queue is a path
    queue = [[root]]
    # visited keeps track of nodes already visited 
    visited = []
    while len (queue) > 0:
        path = queue.pop(0)
        node = path[-1] # last node in the path
        if node not in visited:
            for neighbour in graph[node]:
                # make a copy of the path so far
                new_path = path.copy()
                # add this neighbour to the path we are exploring
                new_path.append(neighbour) 
                if neighbour == query:
                    # we have found the query
                    print("Shortest path = ", " -> ".join(new_path))
                    # stop the search
                    return
                # add the new path to the queue
                queue.append(new_path)
            visited.append(node)
    print("not found")

bfs()