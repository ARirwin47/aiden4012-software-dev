import sys, pygame
from pygame.image import load
from sprites import Bullet, Asteroid, Station, EnemyBullet
from random import random, randint
from pygame.math import Vector2

pygame.init()

spaceship = pygame.image.load("spaceship.png")
sky = pygame.image.load("nightskyemission.png")

screen = pygame.display.set_mode((800, 600))
offset = pygame.display.set_mode((800, 600))

pos = [400, 300]
incr = [0, 0]

bullets = pygame.sprite.Group()


asteroids = pygame.sprite.Group()
station = pygame.sprite.Group()
enemybullet = pygame.sprite.Group()

too_close = False

#pygame.Rect(0, )

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                incr[0] = -1
            if event.key == pygame.K_RIGHT:
                incr[0] = 1
            if event.key == pygame.K_UP:
                incr[1] = -1
            if event.key == pygame.K_DOWN:
                incr[1] = 1
            if event.key == pygame.K_SPACE:
                bullet = Bullet(pos[0], pos[1])
                bullets.add(bullet)
                

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                incr[0] = 0
            if event.key == pygame.K_RIGHT:
                incr[0] = 0
            if event.key == pygame.K_UP:
                incr[1] = 0
            if event.key == pygame.K_DOWN:
                incr[1] = 0
    
        if random() > 0.99:
            asteroids.add(Asteroid(randint(0, 800), 0))
        
        if random() > 0.995:
            station.add(Station(randint(0, 800), 0))

        for s in station:
            if s.position.y <600:
                if random() > 0.99:
                    enemybullet.add(EnemyBullet(s.position, pos))

        #if pygame.sprite.spritecollideany(spaceship, asteroids):
            #spaceship.explode()
        
        #if pygame.sprite.spritecollideany(spaceship, enemybullet):
            #spaceship.explode()

        #if pygame.sprite.spritecollideany(station, bullets):
            #station.explode()

        #if asteroids.position.distance_to(new_station_pos) < 100:
            #too_close = True


    pos[0] = pos[0] + incr[0]
    pos[1] = pos[1] + incr[1]

    bullets.update()
    
    pygame.sprite.groupcollide(asteroids, bullets, True, True)
    pygame.sprite.groupcollide(station, bullets, True, True)
    #pygame.sprite.groupcollide()
    #screen.blit(sky,(0,0), window)
    screen.fill((0, 0, 0))
    screen.blit(spaceship,(pos[0], pos[1]))
    
    for b in bullets:
        b.draw(screen)
    
    pygame.display.update()

