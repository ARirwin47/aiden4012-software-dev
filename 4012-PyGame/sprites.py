import pygame
from pygame.sprite import Sprite
from pygame.math import Vector2



class Bullet(Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.rect = pygame.Rect(x, y, 3, 10)
        self.position = Vector2(x, y)
        
    def draw(self, target):
        pygame.draw.rect(target, (100, 100, 100), self.rect)
        pygame.draw.circle(target,(255,0,0,0), self.position, 4)
    
    def update(self):
        self.position.y = self.position.y - 1
        self.rect.center = self.position

class Asteroid(Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load("Asteroid_02.GIF")
        self.rect = self.image.get_rect()
        self.position = Vector2(x, y)
        
    def draw(self, target):
        pygame.draw.rect(target, (100, 100, 100), self.rect)
    
    def update(self):
        self.rect.y = self.rect.y + 1

class Station(Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load("Station.png")
        self.image = pygame.transform.scale(self.image, (100, 100))
        self.rect = self.image.get_rect()
        self.position = Vector2(x, y)
        
    def draw(self, target):
        pygame.draw.rect(target, (100, 100, 100), self.rect)
    
    def update(self):
        self.rect.y = self.rect.y + 1

class EnemyBullet(Bullet):
    def __init__(self, origin, target):
        super().__init__(origin.x, origin.y)
        self.position = origin.copy()
        self.rect = pygame.Rect(origin.x, origin.y, 3, 5)
        self.direction = target - self.position
        self.direction = 5 * self.direction.normalize()

    def update(self):
        self.position = self.position + self.direction
        self.rect.center = self.position

class SpaceShip(Sprite):
    def __init__(self):
        super().__init__()
        self.position = Vector2(400, 300)
        self.direction = Vector2(0, 0)
    
    def update(self):
        self.position += self.direction
        self.rect.center = self.position
    
    def move_right(self):
        self.direction.x = 5
    
    def move_left(self):
        self.direction.x = -5
    
    def move_up(self):
        self.direction.y = 5
    
    def move_down(self):
        self.direction.x = -5

    